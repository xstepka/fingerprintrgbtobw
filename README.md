# fingerprintRGBtoBW

## Structure and installation

The main FIJI/ImageJ script is `fingerprintRGBtoBW.ijm` in the root directory.

Needs a FIJI distribution (https://imagej.net/software/fiji/downloads) in a subdirectory (any name, e.g. `/Fiji`).
Required update sites (add to a default distribution via `Help->Update...`, `Manage update sites`):
- ImageJ (`https://update.imagej.net/`)
- Fiji (`https://update.fiji.sc/`)
- Java-8 (`https://sites.imagej.net/Java-8/`)
- IJ-Plugins (`https://sites.imagej.net/IJ-Plugins/`)
- IJPB-plugins (`https://sites.imagej.net/IJPB-plugins/`)
- UCB Vision Sciences (`https://sites.imagej.net/Llamero/`)

If the Gabor filtering step should be used, needs Python and the content of https://github.com/172454/Fingerprint-Enhancement-Python in the `/Fingerprint-Enhancement-Python-master` subdirectory.

(If different locations should be used, some hard-coded paths will have to be edited inside `fingerprintRGBtoBW.ijm`, and possibly changed from relative to absolute paths.)

## Running the script

Run using the FIJI/ImageJ distribution inside a subdirectory.

Because of the way FIJI/ImageJ browses for files, the script has to be launched using `Plugins->Macros->Edit...` in the main FIJI/ImageJ window,selecting `fingerprintRGBtoBW.ijm`, and then clicking `Run` or `[Ctrl+R]` in the editor window.

(Simple `Plugins->Macros->Run...` WILL NOT WORK, and will instead produce a `Statement cannot begin with '#'` error message.)

(Also, when you open the script, you might see some warnings or errors. These may be related to FIJI/ImageJ updates and relatively safe to ignore.)

Then watch how the fingerprint is processed on the images as they appear. A final file named `Result.tif` will be created and saved next to your photo file (and displayed in the last window). You can check that the process was completed successfully in the log:

```
Opening /home/pv080/FingerprintProcessing/Example.tif
Coin found at (640, 288), radius = 130 pixels, image DPI = 330.2
Finger width: 327.1076, finger scale = 0.6542
Median filtered
Background subtracted
MedianWithoutRidges CED filtered
Starting Gabor filtering (Python script)...
Gabor filtering done
```

You can re-run the process and adjust it using the following parameters:

`CED sigma` – keep small, ~0.5 (corresponds to noise scale)

`CED rho` – try 10..50 (higher values might generate false ridges from noise)

`CED iterations` – try 10..100 (overall level of blurring along the ridges)

Check where the boundaries of the coin and the fingerprint were identified on the `Centroid overlay` intermediate image. If you did not get the expected result (e.g., a wrong part of the finger was found, or the coin was not identified correctly), you can re-run the process and mark the coin and finger manually.

If you are not using any coin and want to mark the finger manually, then you should set up the following:

<img src="./manually_without_coin.png" width="250" />

If you are using a coin and want to mark the finger and coin manually, then you should set up the following:

<img src="./manually_with_coin.png" width="250" />

Then you will be prompted to mark the diameter of the coin manually. Use `line`. Alternatively, you can draw a bounding box instead of the diameter.

CAREFUL: First mark the size, _then_ press `OK`.

<img src="./mark_coin_with_line.png" width="350" />

Then you will be prompted to mark the fingerprint. Use `polygon selection`. Mark only the fingertip (it is better to miss a part of the finger than include the background). If you have long nails, treat them as part of the background (do not include them in the selection).

CAREFUL: First mark the fingertip, _then_ press `OK`.

<img src="./mark_finger_with_polygon.png" width="250" />
<img src="./wrong_marking_example.png" width="200" />

## Input

Sharp RGB image, side-illuminated fingertip on a smooth black background, optionally next to a coin for scale (preferably 1 CZK; otherwise, its diameter in mm will have to be updated in the script or at runtime). There has to be some space between the coin and the fingertip. No other significant edges in the image (e.g. an edge of a table, other fingers).

A coin is used for size estimation. It is important to make sure that the coin is at the same distance from the camera as your fingertip (depending on the focal length, you may have to use multiple coins stacked on top of each other to avoid perspective distortion).

Example of the photo:

<img src="./Photo_example.png" width="350" />

Edited from: https://www.pexels.com/photo/close-up-of-human-hand-327533/

Do not open the image in advance, the script will ask you to browse for it.

## Parameters

Can be specified via GUI (after the first computation that runs with the default values), or modified inside `fingerprintRGBtoBW.ijm`, in the initial section marked as `SETUP BLOCK`.

## Authors

`fingerprintRGBtoBW.ijm`: Karel Štěpka, CBIA FI, Masaryk University (172454 at mail.muni.cz)

`Fingerprint-Enhancement-Python`: Forked from https://github.com/ylevalle/Fingerprint-Enhancement-Python (Yamila Levalle) and updated to work on Python 3.


