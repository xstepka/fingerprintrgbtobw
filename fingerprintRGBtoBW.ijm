//
// fingerprintRGBtoBW.ijm
//
// Karel Stepka
// CBIA FI, Masaryk University
// 172454@mail.muni.cz
//
// 2022
//
//
//
// RUNNING THE SCRIPT:
//
// Because of the way FIJI/ImageJ browses for files, the script has to be
// launched using "Plugins->Macros->Edit..." in the main FIJI window,
// selecting this .ijm file, and then clicking "Run" or "Ctrl+R" in the
// editor window.
//
// (Simple "Plugins->Macros->Run..." WILL NOT WORK, and will instead
//  produce a "Statement cannot begin with '#'" error message.)
//
//
//
// INPUT:
//
// RGB image, side-illuminated finger on a smooth black background, optionally
// next to a coin (preferably 1 CZK) for scale. No other significant edges in
// the image (e.g. an edge of a table, other fingers).
//
// Do not open the image in advance, the script will ask you to browse for it.
//
//
//
// PARAMETERS:
//
// Can be specified via GUI (after the first computation that runs with the
// default values), or modified in the initial section marked as "SETUP BLOCK".
//

// Constants
KEEP_NO_INTERMEDIATE_IMAGES = 0;
KEEP_ONLY_FINGER_AND_COIN_DETECTION = 1;
KEEP_ALL_INTERMEDIATE_IMAGES = 2;
debugLevelDescriptions = newArray("none", "finger + coin", "all");

////////////////////////////////////////////////////////////////////////////////
///////////////////////////// START OF SETUP BLOCK /////////////////////////////

debugLevelOfIntermediateImagesKeptOpen = KEEP_ONLY_FINGER_AND_COIN_DETECTION; // What level of intermediate images should be kept open after the computation?
makeDebugPauses = false;


shouldMarkFingerManually = false;
shouldLookForCoinAutomatically = true;
shouldMarkCoinManually = false; // Manual coin marking is only used if not already looking automatically.
maximumImageSideLength = 1000;

// Select which version(s) of the result you want to save:
shouldSaveNormal = true;
shouldSaveMirrored = true;

shouldRepeat = false; // Immediately offer repeating the process with different parameters?
shouldCloseImages = true; // Close the intermediate images after the computation?
shouldRunGaborFilterEnhancement = true; // Use the external Python script for Gabor filtering as the final step?
shouldInvertResultOfGabor = true;
shouldDeleteIntermediatePNGs = true; // "MedianWithoutRidgesCEDFilteredThresholded.png" and "GaborFiltered.png" PNG files must be created during Gabor filtering, but can be deleted afterwards to clean up.

// The following lines will enable as much manual processing as possible, if no detection seems to work for an image:
//   shouldMarkFingerManually = true;
//   shouldLookForCoinAutomatically = false;
//   shouldMarkCoinManually = true;



// Radius of this median smoothing can be adjusted between 1 and 20 or more, default is 10. Too low may fail to separate the coin from a too noisy background, too high may skip the coin and the finger completely.
medianRadiusToIdentifyCoin = 10;

// Thresholding method used on the image gradient when looking for coin and finger edges. Default is "Otsu", but "Triangle" can also be an useful alternative.
thresholdingMethod = "Otsu";

// Should the gradient contrast be enhanced?
// "true" should be better if the background is not a problem, but the finger could be mistaken for the coin.
// "false" should be better if there is a strong background pattern (belonging neither to the finger, nor the coin).
enhanceGradientContrastBeforeLookingForCoin = true;

// Default values. "Base" means the property is related to the finger size, and the actual value will be multiplied by fingerWidth / baseFingerWidth (actual / expected finger width, defined below).
defaultCoinDiameterInMm = 20; // 1 Czech koruna coin has the diameter of 20 mm.
defaultBaseMedianRadius = 2;
defaultBaseRidgeMedianRadius = 15; // Median radius to completely remove ridges/valleys
defaultBaseRollingBackgroundSize = 25;
defaultBaseCEDSigma  = 0.5; // Common values around 0.5 to 1.0. Noise scale. 
defaultBaseCEDRho  = 10; // Common values 0 to 50, default used to be 50, then lowered to 10. Higher values tend to connect isolated spots into ridges.
defaultBaseCEDIterations = 10; // Common values 10 to 100. Overall level of blurring along the ridges.

////////////////////////////// END OF SETUP BLOCK //////////////////////////////
////////////////////////////////////////////////////////////////////////////////



// Expected finger width in pixels, do not change. All base dimensions are tuned for this finger width and then scaled according to the actual size.
baseFingerWidth = 500;

// Set everything to defaults at start.
coinDiameterInMm = defaultCoinDiameterInMm;
baseMedianRadius = defaultBaseMedianRadius;
baseRidgeMedianRadius = defaultBaseRidgeMedianRadius;
baseRollingBackgroundSize = defaultBaseRollingBackgroundSize;
baseCEDSigma = defaultBaseCEDSigma;
baseCEDRho = defaultBaseCEDRho;
baseCEDIterations = defaultBaseCEDIterations;

// Because of the following line, the script has to be launched using "Plugins->Macros->Edit..."
// in the main FIJI window, selecting this .ijm file, and then clicking "Run" or "Ctrl+R".
//
// (Simple "Plugins->Macros->Run..." WILL NOT WORK, and will instead produce a "Statement cannot
// begin with '#'" error message.)
//
// Alternatively, a simple fixed filename can be used:
// fullFileName = "G:/Work/Fingerprints/RGB Images/Own Left Pinky/RGB Finger.tif";
#@ File (label = "File: ", value = "G:/Work/Fingerprints/RGB Images/Own Left Pinky/RGB Finger.tif") fullFileName
print("\\Clear");

do {
	selectWindow("Log");
	
	// Open the file.
	print("Opening " + fullFileName);
	if (isOpen(fullFileName)) {
		close(fullFileName);
	}
	open(fullFileName);
	rename("RGBInput: " + fullFileName);

	// Resize to a specified maximum side length (maximumImageSideLength)
	rescaleFactor = Math.min(1.0, Math.min(maximumImageSideLength / getWidth(), maximumImageSideLength / getHeight()));	
	run("Scale...", "x=" + rescaleFactor + " y=" + rescaleFactor + " interpolation=Bicubic average create");
	rename("RGBInput resized: " + fullFileName);

	// Convert to grayscale.
	run("Duplicate...", "title=[...computing GrayscaleInput...]");
	run("8-bit"); // Morphology and bilateral filtering will only work with 8-bit images.
	rename("GrayscaleInput");
	GrayscaleInputID = getImageID();

	// Prepare the images for automatic detection of the finger and/or coin.
	if (!shouldMarkFingerManually || shouldLookForCoinAutomatically) {
if (makeDebugPauses) { Dialog.createNonBlocking(".1."); Dialog.show(); }
		// Suppress detailed and contrasting background before looking for the coin.
		run("Duplicate...", "title=GrayscaleInputBlurred");
		run("Median...", "radius=" + medianRadiusToIdentifyCoin); // Radius can be adjusted between 1 and 20 or more. Too low may fail to separate the coin from a too noisy background, too high may skip the coin and the finger completely.
		//run("Gaussian Blur...", "sigma=2"); // Alternative of the previous line, sigma works similarly to radius. Might worsen the finger detection compared to median.
if (makeDebugPauses) { Dialog.createNonBlocking(".2."); Dialog.show(); }

		// Find and measure the coin:
		Stack.setXUnit("pixel");
		Stack.setYUnit("pixel");
		run("Properties...", "channels=1 slices=1 frames=1 pixel_width=1 pixel_height=1 voxel_depth=1");
		run("Gradient (3D)", " ");
		rename("GrayscaleInputBlurredGradient");
if (makeDebugPauses) { Dialog.createNonBlocking(".3."); Dialog.show(); }
		if (enhanceGradientContrastBeforeLookingForCoin) {
			run("Enhance Contrast", "saturated=0.35");
			run("Apply LUT");
		}
		run("Duplicate...", "title=GrayscaleInputBlurredGradientRightAfterThresholding");
		setAutoThreshold(thresholdingMethod + " dark no-reset");
if (makeDebugPauses) { Dialog.createNonBlocking(".4."); Dialog.show(); }
		run("Convert to Mask");
if (makeDebugPauses) { Dialog.createNonBlocking(".5."); Dialog.show(); }
		run("Grays");
		run("Duplicate...", "ThresholdedGradient");
if (makeDebugPauses) { Dialog.createNonBlocking(".6."); Dialog.show(); }
		// Remove possible border artifacts.
		makeRectangle(1, 1, getWidth() - 2, getHeight() - 2);
if (makeDebugPauses) { Dialog.createNonBlocking(".7."); Dialog.show(); }
		run("Clear Outside");	   	
if (makeDebugPauses) { Dialog.createNonBlocking(".8."); Dialog.show(); }
		ThresholdedGradientID = getImageID();
	}

	run("Clear Results");
	coinFound = false;
	if (shouldLookForCoinAutomatically) {
		run("Hough Circle Transform","minRadius=70, maxRadius=300, inc=2, minCircles=1, maxCircles=1, threshold=0.25, resolution=100, ratio=1.0, bandwidth=10, local_radius=10,  reduce show_mask results_table");
		// Wait until Hough transform finishes.
		while (!isOpen("Results")) {
			wait(100);
		}
		// Determine whether a coin has been found.	
		coinFound = getValue("results.count") > 0;
	}

    // Manual marking of the coin.
    coinMarkedManually = false;
	if ((!shouldLookForCoinAutomatically) && (shouldMarkCoinManually)) {
		selectImage(GrayscaleInputID);
		waitForUser("Mark the coin diameter in 'GrayscaleInput' using a selection tool, and press OK when done.\nFor a line selection, the line length will be used.\nOtherwise, Math.max(selectionWidth, selectionHeight) will be used.");
		print("Coin marked manually.");
		coinMarkedManually = true;
	}
if (makeDebugPauses) { Dialog.createNonBlocking(".9."); Dialog.show(); }

    // Determine the image DPI from the coin.
	if (coinFound) {	
		coinRadiusInPixels = getResult("Radius (pixels)");
		coinCentroidX = getResult("X (pixels)");
		coinCentroidY = getResult("Y (pixels)");
		calculatedDPI = coinRadiusInPixels / (0.5 * coinDiameterInMm) * 25.4;
		print("Coin found at (" + coinCentroidX + ", " +coinCentroidY + "), radius = " + coinRadiusInPixels + " pixels, image DPI = " + calculatedDPI);

		// Remove the coin edges from the thresholded gradient.
		selectImage(ThresholdedGradientID);
		coinSafeRadiusMultiplier = 1.2;
		makeOval(coinCentroidX - coinRadiusInPixels * coinSafeRadiusMultiplier,
		         coinCentroidY - coinRadiusInPixels * coinSafeRadiusMultiplier,
		         coinRadiusInPixels * 2 * coinSafeRadiusMultiplier,
		         coinRadiusInPixels * 2 * coinSafeRadiusMultiplier);
		run("Clear", "slice");
	} else if (coinMarkedManually) {
		// Measure the manually marked coin radius. If a linear selection was used, treat it as the diameter. Otherwise, pick the larger side of the selection's bounding box.
		Roi.getBounds(roiX, roiY, roiWidth, roiHeight);
		if (Roi.getType == 'line') {
			coinRadiusInPixels = Math.sqrt(roiWidth * roiWidth + roiHeight * roiHeight) / 2;
		} else {
			coinRadiusInPixels = Math.max(roiWidth, roiHeight) / 2;
		}

		calculatedDPI = coinRadiusInPixels / (0.5 * coinDiameterInMm) * 25.4;
		print("Coin marked manually with radius = " + coinRadiusInPixels + " pixels, image DPI = " + calculatedDPI);
	} else {
		calculatedDPI = 300;
		print("Coin not found, using default image DPI = " + calculatedDPI);
	} 
	if (isOpen("Results")) {
		selectWindow("Results");
		run("Close");
	}
if (makeDebugPauses) { Dialog.createNonBlocking(".10."); Dialog.show(); }



	// Use manual marking of the finger when requested by the user.
	if (shouldMarkFingerManually) {
		selectImage(GrayscaleInputID);
		waitForUser("Mark the finger selection in 'GrayscaleInput' using a selection tool, and press OK when done.");
		print("Finger selection marked manually.");
	} else {
		// Extract the finger edge (largest 8-connected component) and find its convex hull.
		selectImage(ThresholdedGradientID);
		run("Morphological Filters", "operation=Dilation element=Disk radius=1");
if (makeDebugPauses) { Dialog.createNonBlocking(".11."); Dialog.show(); }
		run("Connected Components Labeling", "connectivity=8 type=[16 bits]");
if (makeDebugPauses) { Dialog.createNonBlocking(".12."); Dialog.show(); }
		run("Keep Largest Label");
if (makeDebugPauses) { Dialog.createNonBlocking(".13."); Dialog.show(); }
		run("Convexify");
		rename("FingerMask");
if (makeDebugPauses) { Dialog.createNonBlocking(".14."); Dialog.show(); }
		// Create and store the finger selection.
		setThreshold(0, 1);
		run("Create Selection");
if (makeDebugPauses) { Dialog.createNonBlocking(".15."); Dialog.show(); }
		run("Make Inverse");
if (makeDebugPauses) { Dialog.createNonBlocking(".16."); Dialog.show(); }
	}
	
	roiManager("reset"); // Clear all previous ROIs.
	roiManager("add"); // Add ROI 0, representing the finger in the original image.
	// Measure the finger.
	fingerWidth = getValue("MinFeret"); // Width of the finger in pixels -- all sizes (e.g. Gausssian sigma, structuring element radius...) should be based on this value.
	fingerScale = fingerWidth / baseFingerWidth;
	run("Select None");
	resetThreshold();
	setOption("Changes", false);
	print("Finger width: " + fingerWidth + ", finger scale = " + fingerScale);
	// The image is now 8-bit grayscale with black background, no threshold and no selection is specified.

if (makeDebugPauses) { Dialog.createNonBlocking(".17."); Dialog.show(); }

	// Set the scale units and crop the image to include only the finger.
	selectImage(GrayscaleInputID);
	Stack.setXUnit("inch");
	Stack.setYUnit("inch");
	run("Properties...", "channels=1 slices=1 frames=1 pixel_width=" + 1/calculatedDPI + " pixel_height=" + 1/calculatedDPI + " voxel_depth=1");
	roiManager("Select", 0);
	run("Duplicate...", "title=GrayscaleInputCropped");
	roiManager("add"); // Add ROI 1, representing the finger in the cropped image.

if (makeDebugPauses) { Dialog.createNonBlocking(".18."); Dialog.show(); }

	// Suppress the imaging noise.
	run("Duplicate...", "title=[...computing MedianFiltered...]");
	run("Median...", "radius=" + (baseMedianRadius * fingerScale)); // Should be smaller than the valley/ridge size.
	setOption("Changes", false);
	rename("MedianFiltered" + ": base radius=" + (baseMedianRadius));
	print("Median filtered");

if (makeDebugPauses) { Dialog.createNonBlocking(".19."); Dialog.show(); }


	// Remove the illumination/shading.
	// First, replicate the edge color outside the finger boundary.
	run("Duplicate...", "title=[...computing BackgroundSubtracted...]");
if (makeDebugPauses) { Dialog.createNonBlocking(".20."); Dialog.show(); }
	roiManager("Select", 1);
if (makeDebugPauses) { Dialog.createNonBlocking(".21."); Dialog.show(); }
	run("Make Inverse");
	run("Maximum...", "radius=" + (2 * baseRollingBackgroundSize * fingerScale)); // How far beyond the edge the replication extends.
	run("Select None");
	// Then do the background subtraction.
	run("Subtract Background...", "rolling=" + (baseRollingBackgroundSize * fingerScale));
	roiManager("Select", 1);
if (makeDebugPauses) { Dialog.createNonBlocking(".22."); Dialog.show(); }
	setBackgroundColor(0, 0, 0);
	run("Clear Outside");
	run("Select None");
	setOption("Changes", false);
	rename("BackgroundSubtracted" + ": base rolling=" + (baseRollingBackgroundSize));
	print("Background subtracted");


/* Alternate middle part of the algorithm. Cannot be just uncommented, would need to be properly incorporated in the pipeline.
	// Coherence enhancing diffusion (Weickert 1999).
	run("Duplicate...", "title=[...computing CEDFiltered...]");
	// For figners ~500 pixels wide: run("Coherence Enhancing Diffusion ...", "lambda=0.1 sigma=0.5 rho=10.0 step_size=0.24 m=1.0 number_of_steps=10");
	run("Coherence Enhancing Diffusion ...", "lambda=0.1 sigma=" + (baseCEDSigma * fingerScale) + " rho=" + (baseCEDRho * fingerScale) + " step_size=0.24 m=1.0 number_of_steps=" + (baseCEDIterations * fingerScale));
	setOption("Changes", false);
	CEDFilteredImageID = getImageID();
	rename("CEDFiltered" + ": base lambda=0.1 sigma=" + (baseCEDSigma) + " rho=" + (baseCEDRho) + " step_size=0.24 m=1.0 number_of_steps=" + (baseCEDIterations));
	print("CED filtered");	

	// Attribute bottom hat, valleys become bright
	run("Gray Scale Attribute Filtering", "operation=[Bottom Hat] attribute=[Box Diagonal] minimum=100 connectivity=4"); // TODO: parametrize this "100"
	rename("...computing AttributeBottomHat...");
	setOption("Changes", false);
	rename("AttributeBottomHat");
	print("Attribute bottom hat computed");

	// HMAX filtering.
	//selectWindow("CEDFiltered");
	selectImage(CEDFilteredImageID);
	run("Duplicate...", "title=CEDFilteredMask");
	run("Duplicate...", "title=CEDFilteredMarker");
	run("Subtract...", "value=10"); // TODO: parametrize the required maxima height
	run("Morphological Reconstruction", "marker=CEDFilteredMarker mask=CEDFilteredMask type=[By Dilation] connectivity=4");
	rename("HMAXFiltered");
	close("CEDFilteredMask");
	close("CEDFilteredMarker");
	print("HMAX filtered");
/**/


	// Suppress the ridges and subtract the result from the grayscale.
	selectImage("GrayscaleInputCropped");
	run("Duplicate...", "title=[...computing MedianWithoutRidges...]");
	run("Median...", "radius=" + (baseRidgeMedianRadius * fingerScale)); 
	medianWithoutRidgesTitle = "MedianWithoutRidges" + ": base radius=" + (baseRidgeMedianRadius);
	rename(medianWithoutRidgesTitle);
	setOption("Changes", false);
	imageCalculator("Subtract create", "GrayscaleInputCropped", medianWithoutRidgesTitle);
	setOption("Changes", false);
	rename("MedianWithoutRidgesSubtracted");
	run("Enhance Contrast", "saturated=0.35");

	// Coherence enhancing diffusion (Weickert 1999).
	run("Duplicate...", "title=[...computing MedianWithoutRidges CEDFiltered...]");
	// For figners ~500 pixels wide: run("Coherence Enhancing Diffusion ...", "lambda=0.1 sigma=0.5 rho=10.0 step_size=0.24 m=1.0 number_of_steps=10");
	run("Coherence Enhancing Diffusion ...", "lambda=0.1 sigma=" + (baseCEDSigma * fingerScale) + " rho=" + (baseCEDRho * fingerScale) + " step_size=0.24 m=1.0 number_of_steps=" + (baseCEDIterations * fingerScale));
	setOption("Changes", false);
	MedianWithoutRidgesCEDFilteredImageID = getImageID();
	rename("MedianWithoutRidges CEDFiltered" + ": base lambda=0.1 sigma=" + (baseCEDSigma) + " rho=" + (baseCEDRho) + " step_size=0.24 m=1.0 number_of_steps=" + (baseCEDIterations));
	print("MedianWithoutRidges CED filtered");	

	run("Duplicate...", "title=[Result without Gabor filtering]");
	setAutoThreshold("Li dark no-reset");
	run("Convert to Mask");
	run("Grays");

	// Save the pre-Gabor results.
	resultSavedMessage = "Result has been saved as";
	if (shouldSaveNormal) {
		save(File.directory + "ResultWithoutGaborFiltering.tif");
		resultSavedMessage = resultSavedMessage + "\n" + File.directory + "ResultWithoutGaborFiltering.tif";
	}
	if (shouldSaveMirrored) {
		run("Duplicate...", "title=[Result without Gabor filtering mirrored]");
		run("Flip Horizontally");
		save(File.directory +  "ResultWithoutGaborFilteringMirrored.tif");
		resultSavedMessage = resultSavedMessage + "\n" + File.directory +  "ResultWithoutGaborFilteringMirrored.tif";
		close();
	}
		

	// Save the image as PNG and process it with the python code for Gabor-based filtering.
	if (shouldRunGaborFilterEnhancement) {
		MedianWithoutRidgesCEDFilteredThresholdedFileName = File.directory + "MedianWithoutRidgesCEDFilteredThresholded.png";
		save(MedianWithoutRidgesCEDFilteredThresholdedFileName);
		GaborFilteredFileName = File.directory + "GaborFiltered.png";
		fijiAbsolutePath = File.getAbsolutePath(""); // getDirectory("startup"); can also be used to get the FIJI directory.
		pythonScriptPath = fijiAbsolutePath + "/../Fingerprint-Enhancement-Python-master/src/main_enhancement.py";

		print("Starting Gabor filtering (Python script)...");
		exec("python",
	     	pythonScriptPath, // Path to main_enhancement.py.
	     	MedianWithoutRidgesCEDFilteredThresholdedFileName, // Path to the input for Gabor filtering.
	     	GaborFilteredFileName); // Path to store the output of Gabor filtering.
	
		if (File.exists(GaborFilteredFileName)) { // TODO: It is possible that this only finds an already existing, older file (hiding any possible problems with the Python script). Might want to check the time stamp of the file.
			print("Gabor filtering done");
			open(GaborFilteredFileName);
			setOption("BlackBackground", true);
			run("Convert to Mask");	
			rename("Gabor filtered");

			if (shouldInvertResultOfGabor) {
				run("Invert");
				roiManager("Select", 1);
				setBackgroundColor(0, 0, 0);
				run("Clear Outside");
			}
			save(GaborFilteredFileName);
			Stack.setXUnit("inch");
			Stack.setYUnit("inch");
			run("Properties...", "channels=1 slices=1 frames=1 pixel_width=" + 1/calculatedDPI + " pixel_height=" + 1/calculatedDPI + " voxel_depth=1");

			// Save the post-Gabor results.
			resultSavedMessage = "Result has been saved as";
			if (shouldSaveNormal) {
				save(File.directory +  "Result.tif");
				resultSavedMessage = resultSavedMessage + "\n" + File.directory +  "Result.tif"; 
			}		
			if (shouldSaveMirrored) {
				run("Duplicate...", "title=[Gabor filtered mirrored]");
				run("Flip Horizontally");
				save(File.directory +  "ResultMirrored.tif");
				resultSavedMessage = resultSavedMessage + "\n" + File.directory +  "ResultMirrored.tif"; 
				close();
			}

			// Delete GaborFilteredFileName and MedianWithoutRidgesCEDFilteredThresholdedFileName if not needed.
			if (shouldDeleteIntermediatePNGs) {
				GaborInputDeletedSuccessfully = File.delete(MedianWithoutRidgesCEDFilteredThresholdedFileName);
				GaborOutputDeletedSuccessfully = File.delete(GaborFilteredFileName);
			}		
		} else {
			print("Gabor filtering not successful (check the Python part of the script). Only a result with the basic CED filtering is available.");
		}
	}

	// Close the detailed debug images if not wanted.
	if (debugLevelOfIntermediateImagesKeptOpen < KEEP_ALL_INTERMEDIATE_IMAGES) {
		close("RGBInput*");
		close("GrayscaleInput*");
		close("ThresholdedGradient*");
		//close("Centroid*");
		close("FingerMask");
		close("MedianFiltered*");
		close("BackgroundSubtracted*");
		close("CEDFiltered*");
		/* Closing the intermediate outputs of the alternate middle part of the algorithm.
		close("AttributeBottomHat*");
		close("HMAXFiltered*"); */
		close("MedianWithout*");		
		if (shouldRunGaborFilterEnhancement) {
			close("Result without Gabor filtering");
		}
		
		close("ROI Manager");
	}
	// Close finger and coin detection debug images if not wanted.
	if (debugLevelOfIntermediateImagesKeptOpen < KEEP_ONLY_FINGER_AND_COIN_DETECTION) {
		close("Centroid*");
	}

	// Offer to close all the images, or keep them open (allow re-computing the images with different parameters).
	Dialog.createNonBlocking("Fingerprint result images are ready");
	Dialog.addMessage("Next computation parameters:");
	                  
	Dialog.addCheckbox("Mark finger manually? (default: no)", shouldMarkFingerManually);
	Dialog.addCheckbox("Look for coin automatically? (default: yes)", shouldLookForCoinAutomatically);
	Dialog.addCheckbox("Enh. gradient contrast for coin? (default: yes)", enhanceGradientContrastBeforeLookingForCoin);
	Dialog.addCheckbox("Mark coin manually if not looking? (default: no)", shouldMarkCoinManually);

	Dialog.addNumber("Coin diameter in mm (1 Kc = 20 mm)", coinDiameterInMm);
	Dialog.setInsets(15, 0, 3);
	Dialog.addNumber("Maximum image side length (default: 1000)", maximumImageSideLength);

	Dialog.addNumber("Smooth median radius (base)", baseMedianRadius);
	Dialog.addNumber("Rolling background size (base)", baseRollingBackgroundSize);
	Dialog.addNumber("CED sigma (base, ~0.5)", baseCEDSigma);
	Dialog.addNumber("CED rho (base, ~10..50)", baseCEDRho);
	Dialog.addNumber("CED iterations (base, ~10..100)", baseCEDIterations);
	Dialog.addNumber("Ridge median radius (base)", baseRidgeMedianRadius);
	
	Dialog.addMessage("Gabor filtering (requires Python):");
	Dialog.addCheckbox("Gabor filter enhancement? (default: yes)", shouldRunGaborFilterEnhancement);
	Dialog.addCheckbox("Invert result of Gabor filter? (default: yes)", shouldInvertResultOfGabor);	

	Dialog.addChoice("Intermediate images to keep open", debugLevelDescriptions, debugLevelDescriptions[debugLevelOfIntermediateImagesKeptOpen]);

	Dialog.setInsets(20, 20, 0);
	Dialog.addMessage(  "----------------------------------------------------------------------\n \n"
	                  + resultSavedMessage + "\n \nYou can work with the images now.\n \n"
	                  + "OK to close the images and/or repeat the computations.\n"
	                  + "Cancel to end (same as don't close, don't repeat, OK).");

	Dialog.setInsets(15, 20, 0);
	Dialog.addCheckboxGroup(1, 2, newArray("Close remaining intermediate images?", "Repeat?"), newArray(shouldCloseImages, shouldRepeat));

	
	Dialog.show();
	
	// Get the user-specified values for the next run.
	shouldMarkFingerManually = Dialog.getCheckbox();
	shouldLookForCoinAutomatically = Dialog.getCheckbox();
	enhanceGradientContrastBeforeLookingForCoin = Dialog.getCheckbox();
	shouldMarkCoinManually = Dialog.getCheckbox();

	coinDiameterInMm = Dialog.getNumber();
	maximumImageSideLength = Dialog.getNumber();
	
	baseMedianRadius = Dialog.getNumber();
	baseRollingBackgroundSize = Dialog.getNumber();
	baseCEDSigma = Dialog.getNumber();
	baseCEDRho = Dialog.getNumber();
	baseCEDIterations = Dialog.getNumber();
	baseRidgeMedianRadius = Dialog.getNumber();

	shouldRunGaborFilterEnhancement = Dialog.getCheckbox();
	shouldInvertResultOfGabor = Dialog.getCheckbox();

	selectedDebugLevelOfIntermediateImagesKeptOpen = Dialog.getChoice();

	shouldCloseImages = Dialog.getCheckbox();
	shouldRepeat = Dialog.getCheckbox();

	// Determine the selected level of debug images to keep open next time.
	for (debugLevelOfIntermediateImagesKeptOpen = 0;
	     debugLevelOfIntermediateImagesKeptOpen < debugLevelDescriptions.length - 1 && debugLevelDescriptions[debugLevelOfIntermediateImagesKeptOpen] != selectedDebugLevelOfIntermediateImagesKeptOpen;
	     ++debugLevelOfIntermediateImagesKeptOpen) {}

	if (shouldCloseImages) {
		print("Closing images");

		close("RGBInput*");
		close("GrayscaleInput*");
		close("ThresholdedGradient*");
		close("Centroid*");
		close("FingerMask");
		close("MedianFiltered*");
		close("BackgroundSubtracted*");
		close("CEDFiltered*");
		/* Closing the intermediate outputs of the alternate middle part of the algorithm.
		close("AttributeBottomHat*");
		close("HMAXFiltered*"); */
		close("MedianWithout*");
		if (shouldRunGaborFilterEnhancement) {
			close("Result without Gabor filtering");
		}
		
		close("ROI Manager");
	} else {
		print("Keeping images open");
	}
	print("");

} while (shouldRepeat);
print("DONE");










////////////////////////////////////////////////////////////////////////////////
/////// POSSIBLE OTHER ALGORITHMS TO EXPLORE IF DEVELOPING THIS FURTHER ////////


/*
print("START bilateral");
// Showcase bilateral filtering with different parameters.
rangeRadiusValues = newArray(10, 20, 30, 40, 50, 60, 70, 80, 90, 100);
spatialRadiusValues = newArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
selectWindow("GrayscaleInput");
//setBatchMode("hide");
setBatchMode(true); // setBatchMode(true); // Version with "setBatchMode(true);" doesn't hide GrayscaleInput, but then it's not easy to see that the macro is still running.
for (i = 0; i < rangeRadiusValues.length; ++i) {
	for (j = 0; j < spatialRadiusValues.length; ++j) {	
		bilateralFilterRangeRadius = rangeRadiusValues[i];
		bilateralFilterSpatialRadius = spatialRadiusValues[j];
		selectWindow("GrayscaleInput");
		run("Bilateral Filter", "spatial=" + bilateralFilterSpatialRadius + " range=" + bilateralFilterRangeRadius);
		rename("GrayscaleInputBilateralFiltered-spatial" + bilateralFilterSpatialRadius + "range" + bilateralFilterRangeRadius);				
	}
}
// Join all images with titles matching "GrayscaleInputBilateralFiltered" into one stack. Then convert it into a hyperstack.
run("Images to Stack", "name=BilateralFilteredStack title=GrayscaleInputBilateralFiltered use");
run("Stack to Hyperstack...", "order=xyczt(default) channels=1 slices=" + spatialRadiusValues.length + " frames=" + rangeRadiusValues.length + " display=Color");
setBatchMode("exit and display");
print("END bilateral");
/**/



/*
print("START CLAHE");
// Showcase CLAHE with different parameters.
blocksizeValues = newArray(3, 7, 15, 31, 63, 127);
maximumValues = newArray(4, 8, 16, 32, 64, 128);
//blocksizeValues = newArray(7, 15);
//maximumValues = newArray(8, 16);
selectWindow("GrayscaleInput");
//setBatchMode("hide");
setBatchMode(true); // setBatchMode(true); // Version with "setBatchMode(true);" doesn't hide GrayscaleInput, but then it's not easy to see that the macro is still running.
for (i = 0; i < blocksizeValues.length; ++i) {
	for (j = 0; j < maximumValues.length; ++j) {	
		claheBlocksize = blocksizeValues[i];
		claheMaximum = maximumValues[j];
		selectWindow("GrayscaleInput");
		run("Duplicate...", " ");
		run("Enhance Local Contrast (CLAHE)", "blocksize=" + claheBlocksize + " histogram=256 maximum=" + claheMaximum + " mask=*None*");

		rename("CLAHE-blocksize" + claheBlocksize + "maximum" + claheMaximum);				
	}
}
// Join all images with titles matching "CLAHE" into one stack. Then convert it into a hyperstack.
run("Images to Stack", "name=CLAHEStack title=CLAHE use");
run("Stack to Hyperstack...", "order=xyczt(default) channels=1 slices=" + maximumValues.length + " frames=" + blocksizeValues.length + " display=Color");
setBatchMode("exit and display");
print("END CLAHE");
/**/








